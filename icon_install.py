#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import subprocess as sp

username = sp.check_output('whoami', shell=True)
username = username.decode('utf-8').strip('\n')

script_path = os.path.dirname(os.path.realpath(__file__))
path = f"{script_path}/shootin_stars.x86"
install_path = f"/home/{username}/.local/share/applications/shootin_stars.desktop"

icon_file = f"""[Desktop Entry]
Name=Shootin Stars
Comment=Endless runner type game for the PP
Type=Application
Icon={script_path}/logo.svg
Exec={path}
Categories=Godot;Games;
X-Purism-FormFactor=Workstation;Mobile;
"""

with open(install_path, "w+") as f:
    f.write(icon_file)
