extends Node2D
"""
MAKE A SHELL SCRIPT FOR INSTALL THAT USES TAR/GUNZIP THEN MAKES A DESKTOP ICON

powerup for new weapon?

powerdown for increasing cooldown
powerdown for decreasing fire rate
"""

var asteroid = preload("res://Scenes/Asteroid.tscn")
var MISSILE_POWERUP = preload("res://Scenes/MissilePowerup.tscn")
var SLOWDOWN = preload("res://Scenes/SlowDownHeat.tscn")
var LASER = preload("res://Scenes/LaserPowerup.tscn")
var x = 0
var spawn_powerup = false

func _ready():
	pass

func _physics_process(delta):
	pass

func _on_SpawnTimer_timeout():
	var new_spawn_time = $SpawnTimer.get_wait_time()
	var ast = asteroid.instance()
	add_child(ast)
	ast.position = $Spawner.position

	var area = $SpawnArea
	var position = area.rect_position + Vector2(randf() * area.rect_size.x,randf() * area.rect_size.y)
	$Spawner.position = position
	if Global.score % 10 == 0 and Global.score != 0:
		spawn_powerup = true
		if spawn_powerup == true and x == 0:
			var rand_num = null
			if Global.laser_weapon == true:
				rand_num = randi()%2+1
			else:
				rand_num = randi()%3+1
			randomize()
			print(rand_num)
			if rand_num == 1:
				var missile_powerup = MISSILE_POWERUP.instance()
				add_child(missile_powerup)
				missile_powerup.position = $Spawner.position
			elif rand_num == 2:
				var slow_down = SLOWDOWN.instance()
				add_child(slow_down)
				slow_down.position = $Spawner.position
			elif rand_num == 3:
				var laser = LASER.instance()
				add_child(laser)
				laser.position = $Spawner.position
			print(rand_num)
			spawn_powerup = false
			$SpawnTimer.set_wait_time(new_spawn_time * .92)
			Global.heat_mult += .2
			x = 1
			$PowerupTimer.start()
	$SpawnTimer.start()


func _on_PowerupTimer_timeout():
	spawn_powerup = true
	x = 0
