extends Node

var missile_speed = 800
var player_speed = 500
var over_heat = 0
var heat_mult = 1.0
var score = 0
var laser_weapon = false

func reset():
	score = 0
	missile_speed = 800
	player_speed = 500
	over_heat = 0
