extends Button

onready var pause_screen = get_node("../PauseScreen")

func _on_PauseButton_pressed():
	if get_tree().paused == true:
		get_tree().paused = false
		pause_screen.visible = false
	else:
		get_tree().paused = true
		pause_screen.visible = true
