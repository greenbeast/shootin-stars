extends Area2D

var velocity = Vector2()
var powerup_speed = 100

func _physics_process(delta):
	velocity.y = (powerup_speed * delta)
	translate(velocity)

func _on_Timer_timeout():
	queue_free()

func _on_LaserPowerup_body_entered(body):
	if body.name == "Player":
		Global.laser_weapon = true
		queue_free()
