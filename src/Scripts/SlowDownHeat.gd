extends Area2D

var velocity = Vector2()
var powerup_speed = 100

func _physics_process(delta):
	velocity.y = (powerup_speed * delta)
	translate(velocity)
	
func _on_SlowDownHeat_body_entered(body):
	if body.name == "Player":
		Global.heat_mult -= .2
		queue_free()

func _on_DespawnTimer_timeout():
	queue_free()
