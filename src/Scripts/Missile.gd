extends Area2D

var velocity = Vector2()

func _ready():
	pass

func _physics_process(delta):
	velocity.y = -(Global.missile_speed * delta)
	translate(velocity)

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func _on_Missile_body_entered(body):
	if body.is_in_group("Asteroid"):
		body.explode()
		queue_free()
