extends Area2D

var velocity = Vector2()
var powerup_speed = 100

func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	velocity.y = (powerup_speed * delta)
	translate(velocity)

func _on_MissilePowerup_body_entered(body):
	if body.name == "Player":
		Global.missile_speed *= 1.25
		queue_free()


func _on_DespawnTimer_timeout():
	queue_free()
