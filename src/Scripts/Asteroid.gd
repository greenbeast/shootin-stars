extends RigidBody2D

export var asteroid_mass = 10

const ROTATION_SPEED = 0

func _ready():
	# doesn't seem to work
	#set_mass(asteroid_mass)
	pass

func _process(delta):
	rotation += ROTATION_SPEED * delta

func _on_Area2D_body_entered(body):
	if body.name == "Player":
		#print("You died, fool")
		get_tree().change_scene("res://Scenes/Death.tscn")

func explode():
	$CPUParticles2D.set_emitting(true)
	$AnimatedSprite.play("Explosion")
	$ParticleTimer.start()


func _on_ParticleTimer_timeout():
	queue_free()
	Global.score += 5


func _on_DespawnTimer_timeout():
	queue_free()
	#print("Disapeared")
