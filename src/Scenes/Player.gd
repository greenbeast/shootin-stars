extends KinematicBody2D

"""
PP has a 18:9 aspect ratio so maybe have this be 
height of 720 and width of 360?
"""

export var stop_distance = 50
var MISSILE = preload("res://Scenes/Missile.tscn")
var LASER = preload("res://Scenes/Laser.tscn")
var timer_active = false
var cooldown = false

func _process(delta):
	_follow_mouse()
	if timer_active == false and cooldown == false:
		if Input.is_action_pressed("blaster"):
			if Global.laser_weapon == false:
				var left_missile = MISSILE.instance()
				get_parent().add_child(left_missile)
				left_missile.position = $LeftBlaster.global_position

				var right_missile = MISSILE.instance()
				get_parent().add_child(right_missile)
				right_missile.position = $RightBlaster.global_position
				Global.over_heat += 3 * Global.heat_mult
				
			else:
				var laser_1 = LASER.instance()
				get_parent().add_child(laser_1)
				laser_1.position = $LeftBlaster.global_position

				var laser_2 = LASER.instance()
				get_parent().add_child(laser_2)
				laser_2.position = $LeftMiddleBlaster.global_position

				var laser_3 = LASER.instance()
				get_parent().add_child(laser_3)
				laser_3.position = $CenterBlaster.global_position

				var laser_4 = LASER.instance()
				get_parent().add_child(laser_4)
				laser_4.position = $RightMiddleBlaster.global_position

				var laser_5 = LASER.instance()
				get_parent().add_child(laser_5)
				laser_5.position = $RightBlaster.global_position

				Global.over_heat += 3.3 * Global.heat_mult

			$ShootTimer.start()
			timer_active = true
	if Global.over_heat >= 100:
		cooldown = true

	if cooldown == true:
		Global.over_heat -= .5
		if Global.over_heat < 0 or Global.over_heat == 0:
			Global.over_heat == 0
			cooldown = false

func _follow_mouse():
	if position.distance_to(get_global_mouse_position()) > stop_distance:
		var direction = get_global_mouse_position() - position
		var normalized_direction = direction.normalized()
		move_and_slide(normalized_direction * Global.player_speed)

func _on_ShootTimer_timeout():
	timer_active = false
