#+title: Shootin Stars
#+author: Hank Greenburg
#+date: 09-21-2021

* About
Wave based endless runner type game built for the Pine Phone. Try to survive as long as you can without your ship being destroyed!


* Dependencies
 - None, just down, extract and run!

   
* Installation
 - WIP, having issues getting this to work with a =.desktop= file, MR are welcome!

   
* License
MIT


* Icons
I also got the icons I used from [[https://www.freepik.com][freepik at flaticon]] and [[https://www.flaticon.com/authors/smashicons][smashicons]]!

* Screenshots
[[Screenshots/SS_1.png]]


[[Screenshots/SS_2.png]]


[[Screenshots/ss_3.png]]
